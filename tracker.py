#!/usr/bin/env python2
# -*- coding: utf-8 -*-

class Tracker(object):
    """Queries the tracker and returns a list containing all the results
        that match all the patters in the pattern list.
        The each result is a tuple (Title, Magnet, TorrentURL)."""
    def search(self, patterns):
        raise NotImplementedError("Abstract method not implemented")
