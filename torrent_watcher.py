#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import os
import sys
import time
import json
from kat import Kickass

from deluge.ui.client import client
from twisted.internet import reactor
import logging
logging.getLogger('deluge').addHandler(logging.NullHandler())


SHOWS_JSON='tv_series.json'
SECONDS_IN_AN_HOUR = 3600

def load_shows_json():
    d = {}
    if os.path.isfile(SHOWS_JSON):
        with open(SHOWS_JSON, 'r') as f:
            d = json.load(f)
    return d

def get_ep_torrent_url(title, se_no, ep_no, extra=None):
    t = Kickass()
    season_ep = "S{:02}E{:02}".format(se_no, ep_no)
    query = [title, season_ep] + extra
    res = t.search(query)

    for e in res:
        # if there is a direct url to the torrent, return it
        if e[2] is not '':
            return e[2]
        else:
            # otherwise return the magnet
            return e[1]
    return None


def main():
    shows = load_shows_json()
    if len(shows) == 0:
        print "No show to download."
        return

    done = False
    delay = SECONDS_IN_AN_HOUR / 2
    delay = delay / len(shows)
    i = 0

    while len(shows) != 0:
        time.sleep(delay)

        curr = shows[i]

        url = get_ep_torrent_url(curr['title'], curr['last_season'], curr['last_episode'], extra=curr['keywords'])
        print url
        if url is not None:
            add_torrent(url)
            del shows[i]
            if len(shows) == 0:
                continue
            delay = SECONDS_IN_AN_HOUR / 2
            delay = delay / len(shows)
            i = 0
        else:
            i = (i + 1) % len(shows)


def close():
    client.disconnect()
    reactor.stop()

def on_connect_success(res, url):
    def on_add_torrent(torrent_id):
        close()
    client.core.add_torrent_url(url, {}).addCallback(on_add_torrent)

def on_connect_fail(result):
    print "Connection failed!"
    close()

def add_torrent(url):
    d = client.connect()
    d.addCallback(on_connect_success, url)
    d.addErrback(close, url)
    reactor.run()

if __name__ == '__main__':
    main()
