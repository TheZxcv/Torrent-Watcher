#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from tracker import Tracker
import feedparser

class Kickass(Tracker):
    FEED_RSS_URL_HEAD = "https://kat.cr/usearch/"
    FEED_RSS_URL_TAIL = "/?rss=1"

    def search(self, patterns):
        url = Kickass._make_url(patterns)
        feed = feedparser.parse(url)
        results = []
        for e in feed['entries']:
            results.append((e['title'], e['torrent_magneturi'], e['links'][1]['href']))
        return results


    @staticmethod
    def _make_url(patterns):
        return Kickass.FEED_RSS_URL_HEAD + ' '.join(patterns) + Kickass.FEED_RSS_URL_TAIL
