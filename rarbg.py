#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from tracker import Tracker
import json
import requests

class Rarbg(Tracker):
    API_URL = 'https://torrentapi.org/pubapi_v2.php'

    def search(self, patterns):
        token = Rarbg._get_token()
        params = Rarbg._prepare_params(patterns, token)
        req = requests.get(Rarbg.API_URL, params=params)
        res = json.loads(req.text)

        results = []
        for e in res['torrent_results']:
            results.append((e['filename'], e['download'], ''))

        return results


    @staticmethod
    def _get_token():
        req = requests.get(Rarbg.API_URL, params={'get_token' : 'get_token'})
        d = json.loads(req.text)
        return d['token']

    @staticmethod
    def _prepare_params(patterns, token):
        d = {}
        d['mode'] = 'search'
        d['search_string'] = ' '.join(patterns)
        d['token'] = token
        return d
